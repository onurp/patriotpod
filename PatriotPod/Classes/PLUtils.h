//
//  PLUtils.h
//  Pods
//
//  Created by Onur Avsar on 04/05/16.
//
//

#import <Foundation/Foundation.h>

@interface PLUtils : NSObject

+ (BOOL)isValidEmail:(UITextField *)textfield;
+ (BOOL)isValidIdentity:(NSString *)identity;

@end
