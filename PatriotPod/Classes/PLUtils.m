//
//  PLUtils.m
//  Pods
//
//  Created by Onur Avsar on 04/05/16.
//
//

#import "PLUtils.h"

@implementation PLUtils

+ (BOOL)isValidEmail:(UITextField *)textfield {
    BOOL stricterFilter = YES;
    NSString *stricterFilterString = @"^[A-Z0-9a-z\\._%+-]+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2,4}$";
    NSString *laxString = @"^.+@([A-Za-z0-9-]+\\.)+[A-Za-z]{2}[A-Za-z]*​$";
    NSString *emailRegex = stricterFilter ? stricterFilterString : laxString;
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:textfield.text];
}

+ (BOOL)isValidIdentity:(NSString *)identity {
    NSInteger identityNumber = [identity integerValue];
    NSMutableArray  *numbers =[NSMutableArray new];
    while (identityNumber) {
        NSNumber *number = [NSNumber numberWithInteger:(identityNumber % 10)];
        [numbers insertObject:number atIndex:0];
        identityNumber = identityNumber/10;
    }
    
    NSInteger oddSum=0;
    NSInteger evenSum=0;
    
    for (NSInteger index=0; index<9; index++) {
        if ((index+1)%2 ) {
            oddSum = oddSum + [[numbers objectAtIndex:index] integerValue];
        }
        else{
            evenSum = evenSum + [[numbers objectAtIndex:index] integerValue];
        }
    }
    
    NSInteger tenthNumber = [[numbers objectAtIndex:9] integerValue];
    NSInteger eleventhNumber = [[numbers objectAtIndex:10] integerValue];
    if (((oddSum * 7)-evenSum)%10 == tenthNumber && (oddSum + evenSum+tenthNumber)%10 == eleventhNumber) {
        return YES;
    }
    return NO;
}

@end
